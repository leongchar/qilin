# ENGR 301 Project *Newspaper Digitization* Project Proposal and Requirements Document
#### Andrew McGhie, Deacon McIntyre, Inti Mateus, Rachel Anderson, Tabitha Byrne, Tomas Brown

## 1. Introduction 

### Client

* Rhys Owen:
    * Office: Room 006, Rankine Brown Building, University Library, Kelburn
    * Email: <rhys.owen@vuw.ac.nz>
* Ya-Wen Ho:
    * Office: Room 006, Rankine Brown Building, University Library, Kelburn
    * Email: <ya-wen.ho@vuw.ac.nz>
* Dr. Sydney Shep:
    * Office: Room 006, Rankine Brown Gate 3 Kelburn Pde
    * Email: <sydney.shep@vuw.ac.nz>

### 1.1 Purpose

The purpose of the software is to play a small part in a larger project's aim, which is conserving a historical printing method, used by a Chinese agricultural press in New Zealand. The three steps of this software's process include identification of the Chinese characters in the supplied newspapers, frequency analysis on the characters identified in each issue, and HTML output of the maximum number of each character found in a single newspaper issue. This output will help the clients have a better understanding of the number of sorts for each character in their type-set, which is the issue they are currently facing.

### 1.2 Scope

The software, `Qilin`, will receive a set of Chinese newspapers in TIFF format. It will then categorize each character according to its size and type. Furthermore, it will perform frequency analysis on each value in the found character set, per newspaper issue. Once found, the maximum frequency per issue of each character will be output in HTML format as a frequency table. As a stretch goal, the program may also be able to digitize Chinese texts into a PDF format that can be read by a machine.

Once released as an open source project, the software will be able to be used and modified to digitize other traditional Chinese texts.

### 1.3 Product overview 
#### 1.3.1 Product perspective

The software we are creating will be made up of a few ideas or products already created, but the system as a whole is very similar to OCRopus.

OCRopus is an open source system that analyses and performs optical character recognition on documents. 

As the main features of OCRopus are almost identical to the requirements of the project, the clients initially tried to use this product to analyse their newspapers. 
The main issue they faced was that the OCRopus system was created for Latin-based languages, so it did not perform very well.

Although OCRopus seemed to be the perfect fit on paper, the system lacked these capabilities:

1) Distinguishing and separating characters based on font size.

2) Analysing document from top-to-bottom, instead of reading from left-to-right.

3) Recognising strokes and radicals of the Chinese characters.

Our solution is to create our own version of OCRopus, with a frequency analysis extension that handles the output of the system. 

A similar project was done by students of Stanford University, Their software was made to identify the font of weathered Chinese traditional characters and then reconstruct them. The approach used in their project is fairly similar to ours. A neural network was programmed and trained in order to achieve the results required. 

#### 1.3.2 Product functions

The final product will be able to: (In a parallel environment,)

1) download 60GB of newspaper scans from an external cloud storage server,

2) analyse each page from the newspaper and understand which is body text and which is advertisements/notices,

3) pull the characters that are part of the body from the page,

4) perform character recognition on each character found,

5) perform frequency analysis on the Unicode value found for each character, and

6) output the result as a frequency table in HTML format.

#### 1.3.3 User characteristics   

There are two classes of users for our system. The first is the **client** who is the driving force behind the project's creation. The other user of our system is **the public**, as this is an open source project.

Our client,  the Wai-Te-Ata Press, has gained access to The New Zealand Chinese Growers monthly newspaper. Due to a lack of documentation on the sorts used to print these newspapers, the number of sorts corresponding to each character is unknown. The Wai-Te-Ata Press is running a project to catalog the characters used by the newspaper. Through our software and a number of other projects, the Press hope to restore and categorise the sort to the point they can perform small test runs with the historical equipment.

As this software could potentially be used by other people with similar use cases, it was decided to release this product as an open source product. As our clients have a very specific set of requirements, the finished product will be custom made to their problem. This may cause issues for the open source release as the system will most likely have to be altered to perform with another user's data and output requirements.

#### 1.3.4 Limitations

#### Interfaces to other applications:

The system will be a Python pipeline that runs on Apache Spark, integrates custom software, Tesseract and Python dependencies and is controlled by the GitLab version control system.

#### Parallel operation:

Our software is required to be run in a parallel environment so the high number of processes can be run at the same time. This will in turn speed up the system's runtime.

#### Higher-order language requirements:

We will be using Python 3 to code our software, and many functions in Python contain higher-order functions such as lambdas.

#### Signal handshake protocol:

Apache Spark will connect to an external cloud server, which will require verification protocols and hidden variables.

#### Quality requirements:

The program is required to be reliable, consistent, and accurate.

## 2. References 

L. Deng, L. Liyi Wang, and Z. Ren, “DengWangRen-ChineseCharacterFontClassificationAndTransformation,” cs229.stanford.edu, 22-Sep-2017. [Online]. Available: http://cs229.stanford.edu/proj2016/report/DengWangRen-ChineseCharacterFontClassificationAndTransformation.pdf. [Accessed: 06-Apr-2018]. 

S. Geagea, S. Zhang, N. Sahlin, F. Hasibi, F. Hameed, E. Rafiyan, M. Ekberg, "Software Requirements Specification." [Online]. Available: http://www.cse.chalmers.se/~feldt/courses/reqeng/examples/srs_example_2010_group2.pdf. [Accessed: 07-Apr-2018].

Staff Writer, "Top 10 Software Development Risks," 14-Jun-2010 [Online]. Available: https://www.itproportal.com/2010/06/14/top-ten-software-development-risks/ [Accessed: 08-Apr-2018]

## 3. Specific requirements 

### 3.1 External interfaces: 

There are three mostly separate groups of users for our system and each will interact with the system in different ways. 

The least technical is the users who are interested in the output of the OCR analysis of the input images. These users will use a web interface to see formatted view of the max frequency of the character font and size that was found. There will also be ways to filter the characters. So the user will be able to quickly find the frequency of the character or characters they want.

The next user is someone such as a project member who wants to run the system to generate the frequency analysis output so that the non technical user can view the results of the OCR. This will mean running a script that will automatically run the OCR over the hadoop environment and put the output on to a gitlab.io page for the gitlab repository. 

Another user is the someone who wants to run the OCR on their own images. To do this they will need to be able to set the system up in their own environment without modification to code. They will also need to be able to set the language that they want to do OCR with so they will need to be able to to supply and train on their own training set. Using Tesseract they have their own way to train it as well as providing pretrained weights for a large section of languges. This means the system will need to require a weights file in gitlab that gets included when the repo is cloned. We will do a similar thing when we create out own OCR solution but we will also need to provide a interface via a script to allow the user to create the weights file.

All the scripts in this project will be wrappers around python code. This will mean that another programmer creating a similar system can import out files and use the api outlined.

Given what the users want to do here are a list of scripts that will be needed and their parameters

* Frequency Analysis - Spark
    * Location of .traineddata for Tesseract (optional)
    * Locations of other weights files for other neural networks (optional)
    * Git repo location (optional)
    * Output will be an update of a hosted website
    
* Frequency Analysis - local
    * Location of images
    * Location of .traineddata for Tesseract (optional)
    * Locations of other weights files for other neural networks (optional)
    * Output will be to stdout
    
* Training script for each neural network that is used
    * Epochs, Batch size, Dropout rate, base learning rate
    * Location out training data
    * Location of output
* Generate HTML from the frequency analysis
    * This will take from stdin and output to stdout
    

### 3.2 Functions: 

#### Use cases for technical users looking to run the system or train the system to work on different data

#### Use Case 1: Running the system parallel on a Spark cluster

This use case is needed for the MVP

The User should be able to run the OCR System on a number of images in parallel by using a Spark cluster and the other API and tools that come with the Spark ecosystem. This means that our frequency analysis has to work in a parallel map/reduce way. This is important as the so that the project can fit in with the existing infrastructure.

##### Preconditions

* The User has a Spark cluster setup
* The images are in the cloud and available for download

##### Flow

1. The User runs a script with the necesary parameters to connect to the Spark cluster and get images from the HDFS
2. The System loads the Images and runs the OCR solution
3. The System does the frequency analysis on the results of the OCR
4. The System generates a HTML page from the frequency
4. The System updates the gitlab.io page with the most recent results

#### Use Case 2: Load custom Tesseract traineddata 

The user should be able to retrain Tesseract so that the OCR will work on different languages.

##### Preconditions

* The user has run the traning process for Tesseract and has a .traineddata file from Tesseract

##### Flow

1. The User copies their .traineddata file over the tesseractData .traineddata file
2. The User calls the script to run the system locally as they normally would
3. The System loads the new traineddata file
4. The System does the OCR
5. The System prints the frequencies to console like normal

##### Alternate flow

1. The User calls the script passing in the location of the different .traineddata file
2. The Use case continues as normal from 3

#### Use Case 3: A User wants to train the custom neural networks themselves

By allowing users to train the neural network(s) themselves the user will be able to use the system on a much wider variety of problems such as on different languages and images with text with a completely different layout (such as Maori newspapers).

##### Preconditions

* The user has some training data that is formatted in the way that the system expects
* The user has some knowledge of Machine Learning and can choose reasonable hyperparameters
* The user has installed dependencies such as tensorflow and has a good enough computer to handle the training data and training process
   
##### Flow

1. The user runs the right training script, passing in the location of the training data, output loaction and the hyperparameters that they want to use
2. The System will train the neural network
3. The System will output a file to the output location that can be used to load the weights back into the neural net

#### Use Case 4: A User can use custom weights files

This works similar to the way retraining Tesseract use case works. 

##### Preconditions

* The User has a properly formatted weights file

##### Flow

1. The User copies their weights file over the existing weights file 
2. The User commits and pushes to their fork
3. The User runs the System on Spark and passes in the location of their fork
3. The System will clone from the fork rather than the normal directory when starting nodes
4. The System will load the different weights file
5. The System will update the gitlab.io page for the fork of the repo

#### Use Cases for people who just want to see the results of the frequency analysis

These Use Cases are for people who are not technical and just want to see the output of the OCR system.
So they relate to displaying data in a readable and consumable format.

#### Use Case 5: A User interested in the outcome of the frequency analysis can view the results

This Use case is required for the MVP

The System will generate a static HTML page that can be displayed to the user through a gitlab.io
site or similar hosting service.
This is very important as it is the only way that the system will store output from the frequency 
analysis and make it available to out clients. 

##### Flow

1. The User goes to the url for the gitlab.io page
2. The System displays generated HTML from the frequency analysis from the OCR
3. End Use Case


### 3.3 Usability Requirements: 

The System will be able to be run through a Spark cluster this will generate a web page that will contain basic analysis about the frequency of characters that it found and the characters that it found on each page. The website should provide an easy way to see the maximum frequency of each type of character. The user will be able to search on all the properties of the characters

The System will be mainly run though command line on a Spark cluster as such we will need to provide descriptive help text and documentation as there will not be a GUI that the user trying to run the system will have.

For running the System locally there will be a number of dependencies that are needed some of these will be able to be installed from a package manager but other ones like Tesseract will not be able to be installed this way and require manual installation before trying to run the system. 

### 3.4 Performance requirements 
#### Minimum Viable Product
##### Static requirements
* The user will need to be able to server the results of the frequency analysis to multiple users  
* It should be able to process 60GB of scanned images in TIFF format. 

##### Dynamic requirements
* At least 95% of characters will be identified as the correct character.
* At least 90% of characters will be identified as the correct character **and** the correct size and typeface.
* The number of sorts should be approximated within a 10% margin of the actual number.

#### Extended Requirements
##### Static requirements
* -

##### Dynamic requirements
* Will use context analysis to improve character identification to 99% correct.


### 3.5 Logical database requirements 

The input data for the system will be in a Hive database in the Spark system. This will be responsible for loading the serving the 60GB of images. However this is the best solution as Hive uses a distributed and will work very well with our System running in a distributed environment on a cluster. Once the system has being finished this data will only be used once for the frequency analysis to run as the results will be saved.

The output data will be the output of the frequency analysis which will contain a unique set of character, size and font mapped to a number the max frequency that it appeared. This data will be in the format of an HTML table so that it can be accessed frequency over a web server. This will be kept until the next time the system is run on a set of input images where the previous results will get overridden.

##### Domain Overview
![Domain Overview Diagram](Domain Overview.png)

**Splitter:** responsible for splitting scanned pages into blocks of text and for splitting blocks of texts into characters and in doing so, identify the size of characters (relative to the page).

**Identifier:** responsible for identifying a character and its typeface using a neural network.

**Issue:** contains information about an issue including pages. Also contains a function for mapping all characters to the number of times they occur in the issue.

**Page:** contains information about a page in an issue, including TextBlocks contained and formatting information. Also contains a method for compiling character list from blocks of text.

**TextBlock:** stores information about a block of text, including orientation, contents, and size.

**Character:** stores information about induvidual characters, including size, typeface, value, and a scan of the character.

### 3.6 Design constraints 

* Should be able to access data from a cloud-based storage system as large amounts of data will be involved.
* Should use Apache Spark for database service in order to divide processing load to reduce time required to process large amounts of data.
* System will be developed in Python for ease of integration with pre-existing tools.
* System should run from a cloud server and be accessible by client from, at a minumum, throughout the university campus.


### 3.7 Software system attributes 

#### Accuracy
It is vital that our software produces accurate results. As mentioned in Section 3.4:

* At least 95% of characters should be identified as the correct character.
* At least 90% of characters should be identified as the correct character and the correct size and typeface.
* The system should estimate character frequency within a 10% margin of the actual frequency.

These strict accuracy measures are in place because without them, our software would not have much meaning. The main purpose of the software is to provide information to the client about the characters, so we need to be able to provide that information with confidence.

#### Reliability
If we define success as processing all pages of Chinese text without crashing, then our system should be 100% reliable in the ideal case. The software should not have many reasons to fail, and should be able to process an image safely - regardless of how correctly it is done. For example, it would be better for the system to categorize a character incorrectly, than for it to crash. Realisticly we can expect the software to fail sometimes regardless, so we can expect a reliaiblity of 95%.

#### Availability
Our system is designed to produce information about Chinese characters in text, and should be available for use by anyone. Anyone should be able to provide their own text to our system, and have it analyse the text and produce frequency information about each character. There are no plans to host the system online, allowing the public to use it directly, but it will be open source, meaning others can download and use the system on their local computers.

#### Security
Our software should follow basic security procesdures, but we do not expect to encounter significant issued in this area. For example we will not be storing or using user information like passwords. The only possible security challenge could be to do with the data that we will be processing. The data currently belongs to our client, and is kept in cloud storage.

#### Maintainability
Because we plan on making the software open source, we will need to make it maintainable, and well written. The software should be modular, to allow individual parts of it to be used on their own, and so that pieces can be updated individually. To allow the code to be more easily maintained, it should be written in a way that allows new functionality to be added without changing a lot of other code.

#### Performance
Performance is of concern for our project because we will be processing a large amount of data. It is estimated that there is 60 gigabytes of data available, and there will be at least 1500 newspaper pages to process. To process the entire data set, it would be ideal if it took less than four hours. To process a single page, it should take no longer than one minute. We will be using Apache Spark to process multiple pages in parallel, which will hugely increase efficiency.

### Reusability
The program should be able to be easily modified to perform tasks with simmilar aspects as the primary design.

### Conceptual Integrity
The software is required to have a consistent style of coding and function and variable naming. In such a manner that all components of the code can be well integrated and have no problem interacting with each other.

### Usability
Our system will have to meet all the user requirements that were given by the client. It will also have to be easy for all users to be able to run the program.

### Testability
Our system has to be easily tested to help debugging the software, leading to it achieving a high rate of accuracy. 


### 3.8 Physical and Environmental Requirements 

Physical Characteristics:

This project is entirely software-based, so there are no physical space or weight requirements.
Computing power is the only real resource we require. We may need access to a computer capable of running our project overnight if the processing takes a long time. We may need multiple computers to spread the load of the software. We could also use a server or multiple servers to do the processing - AWS is an option for this however ITS already has a hadoop cluster that we will be able to use.

### 3.10 Supporting information 

Input and output formats: The software should be able to read .TIFF files at a minimum, and produce a report of the frequencies of characters. The report can be in text or HTML format. As an extension, the software could later be able to read in multiple image file types.

## 4. Verification 

#### 4.1 Basic Verification Methods
These verification methods can be used to verify that the software is working when making changes. For example when verifying that the system works the same locally as it does on a Spark Cluster, one could have run both methods, then use one of the methods below to verify that the system works the same in both cases.

#### 4.1.0 Verifying Frequency Analysis:

The Python script should be able to detect frequencies of characters accurately. The script is expected to predict frequencies accurately, within a 10% margin.
To verify this, we can:

* select a simple page of the newspaper
* manually count the occurrences of some of the characters
* run the same page through our script
* compare the script output to our manually counted results
* ensure that the script's output is within 10% of our manually counted results

Alternatively, we can produce a customized page of characters with a known frequency for *all* characters on the page, and do the same test with that. This would mean that no manual counting would be required.

With this alternative approach, it would be simple to produce a large number of these custom pages of characters, and test the extension functionality which involves running the script on a directory full of images.

#### 4.2 Verification of External Interfaces
To verify that the output of the software is properly displayed in HTML on a webpage, we would run our software, then check the webpage to make sure that it has updated, and displays the data as we expect.
To verify that our software correctly interfaces with Spark we would place some test code in our Gitlab repository, then have a Spark node pull and run that code. If the Spark node can successfully pull and run the code then we would be able to say that that external interface has been verified.

#### 4.3 Use Case Verification
#### 4.3.1 Verification of Use Case 1, Running the system parallel on a Spark cluster
The system should be able to run OCR on a number of images in parallel using Spark. To verify this, we would need to monitor multiple Hadoop nodes to ensure that they each process the images. This could be achieved by adding logging to the code, and having each node log with a unique message. Alternatively, we could log in to multiple nodes at once, and check that they are processing the images when the system is run.

#### 4.3.5 Verification of Use Case 5: Load custom Tesseract traineddata
The user should be able to using a different set of trained data as needed. To verify this, we can run the system on one set of trained data, and record the results. Then we would change the trained data, but keep the images the same, and run the system again. Comparing the first and second results would tell us if the system has used different trained data for the two tests.

#### 4.3.7 Verification of Use Case 7: A User wants to train the custom neural networks themselves
To verify this we would need to train the software from scratch, and ensure that ths system produces a properly trained file. The simplest way to do this involve the following steps:
* Run the software normally, and record the results.
* Clear all training data from the system
* Retrain the system on the same data.
* Run the software again, after retraining.
* Verify that the second output matches the first.
By clearing and retraining the system, we could verify that the system can be retrained and still work.

An alternative verification process:
* Create a test image of characters with known frequencies.
* Retrain the software on a different set of data that uses the same character set as the test image.
* Run the frequency analysis script on the test image, after the software has been retrained
* Verify that the frequencies have been detected correctly (section 4.1.0)
* Possibly repeat this process for several different character sets, for example test it on roman alphabet characters

#### 4.3.9 Verification of Use Case 9: A User can use custom weights files when running the system though hadoop
Similar to Section 4.3.5, but using a weight file instead of a trained data file.

#### 4.3.12 Verification of Use Case 12: A User can pipe the output of frequency analysis to create HTML
To verify this, we would run the system, and verify that results are printed to stdout. Then we would run the system again, and use the script which produces an html output. We would then make sure that the html output matches the regular output.

#### 4.3.13 Use Case 13: A User can start a docker container to quickly test the results
To verify that the docker works, we could load install the necessary files and docker onto a computer that does not have any of our project files, to make sure that they do not interfere. We would then start the docker, and perform several of the use case verifications described above to ensure that the docker has full functionality.

#### 4.4 Verification of Usability Requirements
One of the primary outputs of our software will be information, and we plan on having this information displayed on a webpage for our clients to access easily. To verify that this webpage is simple to use, we would ask the clients for feedback. We would need to verify that the webpage provides all the information that the client expects, including a simple way to view the frequency of each character from the data. In addition, we will need to verify that the page has a working search function, to navigate directly to specific characters. To verify this it is unlikely that we will require client feedback, because we will be able to go to the webpage ourselves to verify that the search function works.

#### 4.5 Verification of Design Constraints
One of our design constraints specifies that our software should be able to access data from cloud-based storage. To verify this, we would have our system select and download a file from the cloud-based storage, and verify that it has ben received correctly. To make sure the file has been received correctly, we could manually or digitially compare the file downloaded by our software against the original on the cloud.
Another design constraint specifies that our client should be able to run our software. To verify this, we would provide the client with instructions to set up our software, then ask them for feedback regarding whether it worked or not, and how easy it was to set up.

#### 4.6 Software System Attributes Verification
#### 4.6.0 Verification of Accuracy
To verify that our results are accurate we will need to have a test image with known frequencies, and compare the output of the software to that test image. We would then be able to calculate a percentage by recording how many characters are identified correctly, and ensure that that percentage is greater than 95. Then we would repeat the process, ensure that the characters are also the right size typeface, with an expected accuracy of 90. This would be a time consuming process, so it would be best to do the test on a test image with a limited amount of characters.

#### 4.6.1 Verification of Reliability
To verify the reliability of our software, we would need to run stress tests on it. These stress tests would involve repeatedly running the software from start to finish, as many times as we can, to ensure that it can withstand a large amount of use. We would record how many times the software crashes or fails in this time, and use that to judge how reliable it is. As an estimate, if we ran the full program five times in a row it should not crash more than once. Another stress test would be to provide the software with a huge amount of data, something like 120 gigabytes if that is plausible. If the software can process that amount of data without failing, that would be an indication that the software is reliable.

#### 4.6.2 Verification of Availability
Verifying availablity of the software would simply involve checking that the software is easily accessibly online, for example in a public git repository. To ensure this, we could navigate to that repository and download the project, then check that it runs as intended after being downloaded.

#### 4.6.3 Verification of Security
To verify that our sofware is secure we could check that the client's data is only accessible by those that should be able to access it. For the most part this means that the data should not be made publicly available. We would also need to make sure that when released to open source, our code does not contain any private information.

#### 4.6.4 Verification of Maintainability
To verify the maintainability of our software, we could split it into its individual modules and ensure that the can function on their own. We could also design a new test function and ensure that it can be be added to the project without changing much else

#### 4.6.5 Verification of Performance
To verify the performance of the software, we could run several timed tests, and ensure that the software completes the tests within our chosen time frames. For example we can test the project on the entire dataset, and verify that it completes in under four hours. We could also provide it with a single page to process, and ensure that it is completed in one minute.

## 5. Development schedule. 

### 5.1 Schedule

Key dates for deliverables will be split into two separate time frames, the first being the Wai Te Ata Press' deliverables and then the courses Submission time frames.
It was agreed by both parties (clients and students) that the time frame given by the course for a prototype and Minimum Viable Product (MVP) is much later than the Press was expecting so we would make two different time frames accordingly.

1. Architectural Prototype: 30th April 2018 (30/04/2018)
  - At this point the system will run on a Spark cluster it will be able to 
      retrieve the input images from the HDFS though the Hive interface as output something to a web server that can server a basic HTML page. 
	- Initially there will be no algorithms or procedures run on the scanned image, the demonstration is to show that the connection between our gitlab and the Hadoop server is working and is a viable solution for the project.
	
1. Minimum Viable Product (MVP): 14th May 2018 (14/05/2018)
	- Our first MVP that will be produced is earlier than the course has defined as the clients would like a version of the program running as soon as possible.
  	- At this point the system will be able to take a scan and make an attempt at recognising the characters. It will not be able to distinguish the size or font of the character.
 	- The System will be able to be run on a cluster managed by Spark
	- Output will be an HTML file that will show a report of how many characters were found on the page categorised by the size of the character.

3. Final Product: 19th October 2018 (19/10/2018)
  - This will use a custom OCR system to boost the accuracy of the recognising the orientation of the text.
  - Be able to recognise the Character size and font accurately
  - Be able to display the output of the frequency analysis in an HTML with filtering for usability
    
### 5.2 Budget

*We came to the conclusion that there is no need for any budget as we will not be spending any money on resources or products.*

## 6. Appendices 
### 6.1 Assumptions and dependencies 

In order for this project to be developed and run in the fashion we are aiming for and that our client would prefer, we will require Spark to be accessible from both ECS machines for the purpose of development on our end and from other VUW computers
so that our clients at Wai-te-ata press can run the software themselves. Our approach is dependent on this, and if this is not met we will have to find a different approach for simultaneously processing the data or in worst case scenario process pages individually. 

We will assume that we will have access permissions, as necessary, in order to obtain the scans of the newspapers we will be performing the analysis on. Though the project can be partially completed without access to this we cannot properly test  it 
without access to these scans so we must assume we are able to do so. If we do not have access we cannot change this requirement so we must gain access to the scans.

We must also assume there will be storage space for the output of the program. The preferred option would be to use cloud storage as analysis of over 60GB data set would result in a similarly large output size. The software being able to run properly is dependent on there being space for the output, so adequate cloud storage or an alternative if absolutely necessary is crucial.

### 6.2 Acronyms and abbreviations

* OCR - Optical Character Recognition, software that identifies text characters from an image
* API - Application Programming Interface
* MVP - Minimum Viable Project
* TIFF - Tagged Image File Format, High quality image format common with scanned images
* PDF - Portable Document Format
* Spark - a parallelisation framework
* Tesseract - A open source OCR program by Google
* OCRopus - another OCR program
* HDFS - Hadoop Distributed File System
* AWS - Amazon Web Services