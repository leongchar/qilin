#!/bin/sh
#
#  filesToHDFS.sh
#sudo hadoop fs -mkdir /root
# remove the existing files from the HDFS if there are any, then replace them with what we have locally
sudo hadoop fs -rm requirements.txt
sudo hadoop fs -copyFromLocal requirements.txt requirements.txt
sudo hadoop fs -rm install.sh
sudo hadoop fs -copyFromLocal install.sh install.sh
