import os


def generate_list():
    files = os.listdir('UTF2')
    files.sort()
    files = [name for name in files if name.endswith(".png")]
    list = open("character_order.txt", "w", encoding="utf-8-sig")
    for file in files:
        character = file[-5]
        list.write(character)
        list.write('\n')


generate_list()
