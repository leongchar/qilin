import re


def get_correct():
    f = open("record.csv", "r", encoding="utf-8-sig").readlines()
    num_correct = 0
    num_total = 0
    for line in f:
        if re.match(r'.*,.*,.*', line):
            correct = line.split(',')[-1][0]
            if correct == '1':
                num_correct += 1
            num_total += 1
    percent_correct = int(num_correct/num_total * 100)
    print("Percentage correct: {}% ({}/{})".format(percent_correct, num_correct, num_total))


get_correct()
