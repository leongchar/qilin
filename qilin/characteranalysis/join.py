def joinMultiple(current, issue):
    for size_name in issue:
        for character in issue[size_name]:
            if size_name not in current:
                current[size_name] = {}
            if character not in current[size_name].keys() \
                    or issue[size_name][character] > current[size_name][character]:
                current[size_name][character] = issue[size_name][character]
    return current
