import unittest
from PIL import Image
from qilin.catalogue_splitter import find_gradient, realign_image, split_rows


class TestSplitter(unittest.TestCase):

    def test_find_gradient(self):
        test_array = [(0, 0), (1, 1)]
        self.assertEqual(find_gradient(test_array), 0.5)

    def test_realign_image(self):
        img_arr = Image.open('resources/catalogue_example.jpeg')
        image_data = realign_image(img_arr, 0, True, "resources/tifs/characters/", save=False)
        self.assertEqual(7, len(image_data))

    def test_split_rows(self):
        img_arr = Image.open('resources/catalogue_example.jpeg')
        image_data = realign_image(img_arr, 0, True, "resources/tifs/characters/", save=False)
        output = split_rows(image_data, True, 0, save=False, save_loc="resources/tifs/characters/")
        self.assertEqual(119, len(output))

if __name__ == '__main__':
    unittest.main()
